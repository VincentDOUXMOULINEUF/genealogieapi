/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genealogie.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Vincent
 */
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "type_document")
class TypeDocument implements Serializable {
    
    @Id
    @Column(name = "type_document_id")
    private Integer type_document_id;
    
    @Column(name = "type_document_libelle")
    private String type_document_libelle;

    public Integer getType_document_id() {
        return type_document_id;
    }

    public void setType_document_id(Integer type_document_id) {
        this.type_document_id = type_document_id;
    }

    public String getType_document_libelle() {
        return type_document_libelle;
    }

    public void setType_document_libelle(String type_document_libelle) {
        this.type_document_libelle = type_document_libelle;
    }

    @Override
    public String toString() {
        return "TypeDocument{" + "type_document_id=" + type_document_id + ", type_document_libelle=" + type_document_libelle + '}';
    }
    
    
}
