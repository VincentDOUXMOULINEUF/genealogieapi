/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genealogie.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Vincent
 */
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "mariage")
public class Mariage implements Serializable {
    
    @Id
    @Column(name = "mariage_id")
    private Integer mariage_id;
    
    @OneToOne (fetch = FetchType.LAZY)
    @JoinColumn(name = "mariage_acte")
    private Document acteMariage;
    
    @Column(name = "mariage_date")
    private String mariage_date;
    
    @Column(name = "mariage_code_postal")
    private String mariage_code_postal;
    
    @Column(name = "mariage_ville")
    private String mariage_ville;
    
    @OneToOne (fetch = FetchType.LAZY)
    @JoinColumn(name = "mariage_personne_1_id")
    private Personne marie;
    
    @OneToOne (fetch = FetchType.LAZY)
    @JoinColumn(name = "mariage_personne_2_id")
    private Personne mariee;

    public Integer getMariage_id() {
        return mariage_id;
    }

    public void setMariage_id(Integer mariage_id) {
        this.mariage_id = mariage_id;
    }

    public Document getActeMariage() {
        return acteMariage;
    }

    public void setActeMariage(Document acteMariage) {
        this.acteMariage = acteMariage;
    }

    public String getMariage_date() {
        return mariage_date;
    }

    public void setMariage_date(String mariage_date) {
        this.mariage_date = mariage_date;
    }

    public String getMariage_code_postal() {
        return mariage_code_postal;
    }

    public void setMariage_code_postal(String mariage_code_postal) {
        this.mariage_code_postal = mariage_code_postal;
    }

    public String getMariage_ville() {
        return mariage_ville;
    }

    public void setMariage_ville(String mariage_ville) {
        this.mariage_ville = mariage_ville;
    }

    public Personne getMarie() {
        return marie;
    }

    public void setMarie(Personne marie) {
        this.marie = marie;
    }

    public Personne getMariee() {
        return mariee;
    }

    public void setMariee(Personne mariee) {
        this.mariee = mariee;
    }

    @Override
    public String toString() {
        return "Mariage{" + "mariage_id=" + mariage_id + ", acteMariage=" + acteMariage + ", mariage_date=" + mariage_date + ", mariage_code_postal=" + mariage_code_postal + ", mariage_ville=" + mariage_ville + ", marie=" + marie + ", mariee=" + mariee + '}';
    }
    
    
}
