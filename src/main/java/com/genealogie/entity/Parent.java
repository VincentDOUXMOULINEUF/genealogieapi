/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genealogie.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Vincent
 */
@Entity(name = "parent")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "parent")
public class Parent implements Serializable {
    
    private static final Logger LOG = Logger.getLogger(Parent.class.getName());

    @Id
    @Column(name = "id_parent")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Integer id;
    
    @OneToOne
    @JoinColumn(name = "id_personne")
    public Personne pers;
    
    @OneToOne
    @JoinColumn(name = "id_pere")
    public Personne pere;
    
    @OneToOne
    @JoinColumn(name = "id_mere")
    public Personne mere;

    public Personne getPers() {
        return pers;
    }

    public Personne getPere() {
        return pere;
    }

    public Personne getMere() {
        return mere;
    }

    public void setPers(Personne pers) {
        this.pers = pers;
    }

    public void setPere(Personne pere) {
        this.pere = pere;
    }

    public void setMere(Personne mere) {
        this.mere = mere;
    }

    @Override
    public String toString() {
        return "Parent{" + "pers=" + pers + ", pere=" + pere + ", mere=" + mere + '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Parent() {
    }

    public Parent(Integer id, Personne pers, Personne pere, Personne mere) {
        this.id = id;
        this.pers = pers;
        this.pere = pere;
        this.mere = mere;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
    
}
