/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genealogie.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Vincent
 */
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "document")
public class Document implements Serializable {
    
    @Id
    @Column(name = "document_id")
    private Integer document_id;
    
    @ManyToOne (fetch = FetchType.LAZY)
    @JoinColumn(name = "personne_personne_id")
    private Personne personneLieeAuDocument;
    
    @ManyToOne (fetch = FetchType.LAZY)
    @JoinColumn(name = "type_document_type_document_id")
    private TypeDocument typeDocument;

    public Integer getDocument_id() {
        return document_id;
    }

    public void setDocument_id(Integer document_id) {
        this.document_id = document_id;
    }

    public Personne getPersonneLieeAuDocument() {
        return personneLieeAuDocument;
    }

    public void setPersonneLieeAuDocument(Personne personneLieeAuDocument) {
        this.personneLieeAuDocument = personneLieeAuDocument;
    }

    public TypeDocument getTypeDocument() {
        return typeDocument;
    }

    public void setTypeDocument(TypeDocument typeDocument) {
        this.typeDocument = typeDocument;
    }

    @Override
    public String toString() {
        return "Document{" + "document_id=" + document_id + ", personneLieeAuDocument=" + personneLieeAuDocument + ", typeDocument=" + typeDocument + '}';
    }
    
    
}
