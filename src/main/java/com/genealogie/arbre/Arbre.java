/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genealogie.arbre;

import com.genealogie.entity.Personne;
import java.util.Objects;

/**
 *
 * @author Vincent
 */
public class Arbre {
    
    public Personne racine;
    public Arbre arbrePere;
    public Arbre arbreMere;

    public Arbre() {
    }

    public Arbre(Personne racine, Arbre arbrePere, Arbre arbreMere) {
        this.racine = racine;
        this.arbrePere = arbrePere;
        this.arbreMere = arbreMere;
    }

    public Personne getRacine() {
        return racine;
    }

    public void setRacine(Personne racine) {
        this.racine = racine;
    }

    public Arbre getArbrePere() {
        return arbrePere;
    }

    public void setArbrePere(Arbre arbrePere) {
        this.arbrePere = arbrePere;
    }

    public Arbre getArbreMere() {
        return arbreMere;
    }

    public void setArbreMere(Arbre arbreMere) {
        this.arbreMere = arbreMere;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Arbre other = (Arbre) obj;
        if (!Objects.equals(this.racine, other.racine)) {
            return false;
        }
        if (!Objects.equals(this.arbrePere, other.arbrePere)) {
            return false;
        }
        if (!Objects.equals(this.arbreMere, other.arbreMere)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Arbre{" + "racine=" + racine + ", arbrePere=" + arbrePere + ", arbreMere=" + arbreMere + '}';
    }
    
    
    
}
