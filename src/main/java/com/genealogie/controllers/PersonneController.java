/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genealogie.controllers;

import com.genealogie.entity.Personne;
import com.genealogie.service.PersonneService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.jboss.logging.Logger;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *
 * @author Vincent
 */
@RestController
@CrossOrigin
@RequestMapping("api/personne")
public class PersonneController {
    
    private static final Logger LOGGER = Logger.getLogger(PersonneController.class);
    
    @Autowired
    public PersonneService personneService;

    @GetMapping(value = "/all")
    public List<Personne> findAll() {
        LOGGER.info("Appel de la méthode \" -- findAll() -- \"");
        return personneService.findAll();
    }
    
    @GetMapping(value = "/{id}")
    public Personne getOne(@PathVariable final Integer id) {
        LOGGER.info("Appel de la méthode pour recherche une personne selon son id");
        return personneService.getOne(id);
    }
    
    @GetMapping(value = "/nom/{nom}")
    public List<Personne> getPersonneByNom(@PathVariable final String nom) {
        LOGGER.info("Appel de la méthode \" -- getPersonneByNom() -- \"");
        return personneService.getPersonneByNom(nom);
    }
    
}
