/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genealogie.controllers;

import com.genealogie.arbre.Arbre;
import com.genealogie.entity.Personne;
import com.genealogie.service.IArbreService;
import java.util.List;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Vincent
 */
@RestController
@CrossOrigin
@RequestMapping("api/arbre")
public class ArbreController {
    
    private static final Logger LOG = Logger.getLogger(ArbreController.class.getName());
    
    @Autowired
    private IArbreService iArbreService;
    
    @GetMapping("/ascendant/{id}")
    public Arbre arbreAscendantPersonne(@PathVariable final Integer id){
        return iArbreService.getArbreAscendantPersonne(id);
    }
    
}

// TODO
// Test de gitlab une nouvelle fois
