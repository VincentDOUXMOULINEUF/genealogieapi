/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genealogie.controllers;

import com.genealogie.entity.Document;
import com.genealogie.entity.Mariage;
import com.genealogie.repositories.IMariageRepository;
import com.genealogie.service.PersonneService;
import java.util.List;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Vincent
 */
@RestController
@CrossOrigin
@RequestMapping("api/mariage")
public class MariageController {
    
    private Logger logger = Logger.getLogger(MariageController.class);
    
    @Autowired
    private IMariageRepository mariageRepo;
    
    @Autowired
    private PersonneService personneService;
    
    @GetMapping(value = "/all")
    public List<Mariage> getAll() {
        for(Mariage mariage : mariageRepo.findAll()) {
            logger.info(mariage.toString());
        }
        List<Mariage> listeMariages = mariageRepo.findAll();
        return listeMariages;
    } 
    
}
