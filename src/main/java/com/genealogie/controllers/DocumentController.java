/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genealogie.controllers;

import com.genealogie.entity.Document;
import com.genealogie.repositories.IDocumentRepository;
import java.util.List;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Vincent
 */
@RestController
@CrossOrigin
@RequestMapping("api/document")
public class DocumentController {
    
    private Logger logger = Logger.getLogger(DocumentController.class);
    
    @Autowired
    private IDocumentRepository documentRepo;
    
    @GetMapping(value = "/all")
    public List<Document> getAll() {
        for(Document doc : documentRepo.findAll()) {
            logger.info(doc.toString());
        }
        return documentRepo.findAll();
    }
    
}
