/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genealogie.controllers;

import com.genealogie.entity.Parent;
import com.genealogie.service.ParentService;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Vincent
 */
@RestController
@CrossOrigin
@RequestMapping("api/parent")
public class ParentController {

    private static final Logger LOG = Logger.getLogger(ParentController.class.getName());
    
    @Autowired
    private ParentService parentService;
    
    @GetMapping(value = "/{id}")
    public Parent getParent(@PathVariable final Integer id) {
        
        return parentService.getOne(id);
    }
    
    
    
    
    
}
