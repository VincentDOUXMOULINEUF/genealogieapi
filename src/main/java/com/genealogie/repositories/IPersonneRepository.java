/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genealogie.repositories;

import com.genealogie.entity.Personne;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

/**
 *
 * @author Vincent
 */
@Component
public interface IPersonneRepository extends JpaRepository<Personne, Integer>{

    @Query(value = "SELECT * FROM personne WHERE personne_nom LIKE %:nom%", nativeQuery = true)
    public List<Personne> getPersonneByNom(@Param("nom") String nom);
    
}
