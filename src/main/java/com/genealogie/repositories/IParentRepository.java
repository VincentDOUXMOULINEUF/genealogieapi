/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genealogie.repositories;

import com.genealogie.entity.Parent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

/**
 *
 * @author Vincent
 */
@Component
public interface IParentRepository extends JpaRepository<Parent, Integer>{

    @Query(value = "SELECT * FROM parent WHERE id_personne = ?1", nativeQuery = true)
    public Parent getParentByPersonneId(Integer id);
   
    
}
