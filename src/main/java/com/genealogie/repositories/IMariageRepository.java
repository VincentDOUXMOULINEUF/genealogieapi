/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genealogie.repositories;

import com.genealogie.entity.Mariage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

/**
 *
 * @author Vincent
 */
@Component
public interface IMariageRepository extends JpaRepository<Mariage, Integer>{
    
}
