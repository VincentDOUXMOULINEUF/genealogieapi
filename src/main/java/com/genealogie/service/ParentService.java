/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genealogie.service;

import com.genealogie.entity.Parent;
import com.genealogie.entity.Personne;
import com.genealogie.repositories.IParentRepository;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Vincent
 */
@Component
public class ParentService implements IParentService {

    private static final Logger LOG = Logger.getLogger(ParentService.class.getName());
    
    @Autowired
    private IParentRepository iParentRepo;
    
    @Override
    public Parent getOne(Integer id) {
        
        LOG.info("Appel de la méthode 'getOne' de IParentService");
        writeLogParent(iParentRepo.getOne(id));
        return iParentRepo.getOne(id);
    }
    
    
    
    public void writeLogParent(Parent parent) {
        LOG.log(Level.INFO, parent.toString());
    }

    @Override
    public Parent getParentByPersonneId(Integer id) {
        return iParentRepo.getParentByPersonneId(id);
        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
}
