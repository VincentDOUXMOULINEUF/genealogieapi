/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genealogie.service;

import com.genealogie.arbre.Arbre;
import com.genealogie.entity.Parent;
import com.genealogie.entity.Personne;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Vincent
 */
@Component
public class ArbreService implements IArbreService {

    private static final Logger LOGGER = Logger.getLogger(ArbreService.class);
    
    @Autowired
    private IPersonneService iPersonneService;
    
    @Autowired
    private IParentService iParentService;
    
    @Override
    public Arbre getArbreAscendantPersonne(Integer id) {
        Arbre arbrePersonne = new Arbre();
        return buildArbre(arbrePersonne, iPersonneService.getOne(id));
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Arbre buildArbre(Arbre arbre, Personne pers) {
        arbre.setRacine(pers);
        LOGGER.info("Ajout de " + pers.toString() + " en racine");
        try {
            Parent parent = iParentService.getParentByPersonneId(pers.getPersonne_id());
            if (parent.pere != null) {
                arbre.setArbrePere(buildArbre(new Arbre(), parent.pere));
                LOGGER.info("Ajout de " + parent.pere.toString() + " en tant que père");
            }
            if (parent.mere != null) {
                arbre.setArbreMere(buildArbre(new Arbre(), parent.mere));
                LOGGER.info("Ajout de " + parent.mere.toString() + " en tant que mère");
            }
        } catch (Exception e){
            return null;
        }       
        return arbre;
    }
    
    
}
