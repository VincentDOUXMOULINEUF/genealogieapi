/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genealogie.service;

import com.genealogie.entity.Personne;
import java.util.List;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.genealogie.repositories.IPersonneRepository;

/**
 *
 * @author Vincent
 */
@Component
public class PersonneService implements IPersonneService {
    
    private static final Logger LOGGER = Logger.getLogger(PersonneService.class);
    
    @Autowired
    public IPersonneRepository iPersonneRepo;

    @Override
    public List<Personne> findAll() {
        LOGGER.info("Appel de la méthode \" -- findAll() -- \" de PersonneService ");
        List<Personne> listPers = iPersonneRepo.findAll();
        for (Personne pers : listPers) {
            writeLogPersonne(pers);
        }
        return listPers;
    }

    @Override
    public Personne getOne(Integer id) {
        LOGGER.info("Appel de la méthode \" -- getOne() -- \" de PersonneService ");
        Personne result = iPersonneRepo.getOne(id);
        writeLogPersonne(result);
        return result;
    }
    
    
    @Override
    public List<Personne> getPersonneByNom(String nom) {
        LOGGER.info("Appel de la méthode \" -- getPersonneByNom() -- \" de PersonneService ");
        List<Personne> listPers = iPersonneRepo.getPersonneByNom(nom);
        for (Personne pers : listPers){
            writeLogPersonne(pers);
        }
        return listPers;
    }
    
    
    public void writeLogPersonne(Personne pers) {
        LOGGER.info("id : " + pers.getPersonne_id() + " -- " + pers.getPersonne_nom() + " " + pers.getPersonne_prenom());
    }

}
