/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genealogie.service;

import com.genealogie.arbre.Arbre;


/**
 *
 * @author Vincent
 */

public interface IArbreService {
    
    public Arbre getArbreAscendantPersonne(Integer id);
}
