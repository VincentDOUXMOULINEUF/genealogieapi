/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genealogie.service;

import com.genealogie.entity.Personne;
import java.util.List;

/**
 *
 * @author Vincent
 */
public interface IPersonneService {
    
    public List<Personne> findAll();
    
    public Personne getOne(Integer id);
    
    public List<Personne> getPersonneByNom(String nom);
}
