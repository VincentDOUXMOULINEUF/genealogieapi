/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genealogie.service;

import com.genealogie.entity.Parent;


/**
 *
 * @author Vincent
 */
public interface IParentService {
    
    public Parent getOne(Integer id);
    
    public Parent getParentByPersonneId(Integer id);
    
}
